import math

#définition des variables des longueurs entre chaque liaisons

L1 = 51									#valeur longueur 1 (bout du premier actionneur)
L2 = 63.7								#valeur longueur 2
L3 = 93									#valeur longueur 3 (bout effecteur)

#définition des coordonnées par défaut (position initiale bout effecteur)

#routine d'initialisation du programme (provoque une erreur de variables globales si inséré dans main()...)

print("\nPosition bout effecteur\n")
x = float(input("position 1 = "))
y = float(input("position 2 = "))
z = float(input("position 3 = "))

#définition des fonctions

def leg_ik(x, y, z, L1, L2, L3):		#formule cinématique patte 3 articulations
	
	Lp = math.sqrt(math.pow(x,2) + math.pow(y,2))	
	
	if Lp > (L1+L2+L3):
		print("point hors portée !!\n")
		return t1, t2, t3

	elif Lp == (L1+L2+L3):
		print("une seule solution possible\n")
	
	else:
		print("deux solutions possibles (on prendra par défaut la valeur positive)\n")
	
	d13 = Lp - L1
	d = math.sqrt(math.pow((Lp-L1),2) + math.pow(z,2))
	if (x > 0 || x < 0):							# si une coordonnée = 0, ne pas faire le calcul (division/0 !!)
		t1 = math.degrees(abs(math.acos(x / Lp)))
	if (y > 0 || y < 0):	
		t2 = math.degrees(abs(math.acos((d13/d) + ((math.pow(d,2)+math.pow(L2,2)-math.pow(L3,2)) / (2*d*L2)))))
	if (z > 0 || z < 0):
		t3 = math.degrees(abs(math.acos((math.pow(L2,2)+math.pow(L3,2)-math.pow(d,2)) / (2*L2*L3))+math.pi))
	return t1, t2, t3

def main():								#code principal
	
	print("\nAngles en degrés:\n")
	
	t1, t2, t3 = leg_ik(x, y, z, L1, L2, L3)		#récup angles
	print("t1 = "+str(x))							#affichage angles
	print("t2 = "+str(y))
	print("t3 = "+str(z))
	
	print("\nLongueurs entre les points:\n")
	print("l1 = "+str(L1))							#affichage longueurs
	print("l2 = "+str(L2))
	print("l3 = "+str(L3))
	
main()									#utilisation main
