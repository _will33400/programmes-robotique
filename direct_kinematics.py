#import itertools
#import time
#import numpy
#import pypot.dynamixel
import math

#définition des variables angulaires patte

O2c = math.radians(20.69)				#valeur corrigée angle 2 en radian
O3c = O2c + math.radians(90 - 5.06)		#valeur corrigée angle 3 en radian
O1 = 0									#valeur basique angle 1
O2 = -O2c								#valeur basique angle 2
O3 = O3c								#valeur basique angle 3

#définition des variables des longueurs entre chaque liaisons

L1 = 51									#valeur longueur 1 (bout du premier actionneur)
L2 = 63.7								#valeur longueur 2
L3 = 93									#valeur longueur 3 (bout effecteur)

#routine d'initialisation du programme (provoque une erreur de variables globales si inséré dans main()...)

print("\nAjustement de l'angle en degrés\n")
O1 += math.radians(float(input("angle 1 = ")))
O2 += math.radians(float(input("angle 2 = ")))
O3 += math.radians(float(input("angle 3 = ")))

#définition des fonctions

def leg_dk(l1, l2, l3, t1, t2, t3):		#formule cinématique patte 3 articulations
	x = (l1*math.cos(t1) + l2*math.cos(t2)*math.cos(t1) + l3*math.cos(t3))*math.cos(t1)
	y = (l1*math.sin(t1) + l2*math.sin(t2)*math.sin(t1) + l3*math.sin(t3))*math.sin(t1)
	z = l2*math.sin(t2) + l3*math.sin(t3)
	return x, y, z

def main():								#code principal
	
	print("\nPosition du bout de l'effecteur dans l'espace en mm:\n")
	
	x, y, z = leg_dk(L1, L2, L3, O1, O2, O3)		#récup coordonnées bout bras
	print("x = "+str(x))							#affichage coordonnées
	print("y = "+str(y))
	print("z = "+str(z))

main()									#utilisation main
